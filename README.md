# PWS verslag

Technische details:
* Geschreven in Madoko
* [Madoko Reference](http://research.microsoft.com/en-us/um/people/daan/madoko/doc/reference.html)
* Madoko installeren: `npm install madoko -g`
* Exporteren: `madoko main.mdk`
  * pdf: `madoko main.mdk --pdf`